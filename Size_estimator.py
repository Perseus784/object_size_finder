import cv2
import numpy as np
import imutils

#image formatting
img=cv2.imread("test.JPG")
img = imutils.resize(img, width=1000, height=60)
hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

#colour ranges of the object
lw_red=np.array([169, 100, 100],dtype="uint8")
up_red=np.array([189, 255, 255],dtype="uint8")
lw_blue=np.array([113, 100, 100],dtype="uint8")
up_blue=np.array([133, 255, 255],dtype="uint8")
min_rad=10

def image_processing(low,upper,text):

    #masking
    mask = cv2.inRange(hsv, low, upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    #finding contours
    contour = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None
    c = max(contour, key=cv2.contourArea)
    M = cv2.moments(c)

    #to find center of the contour
    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

    # boundry
    cv2.drawContours(img, [c], 0, (0, 255, 0), 5)

    # contour center
    cv2.circle(img, center, 5, (0, 0, 255), -1)

    # obatain rectangle corners
    box = cv2.minAreaRect(c)
    box = cv2.boxPoints(box)
    box = np.array(box, dtype='int')
    (bl, tl, tr, br) = box

    #draw outlies for visual functions
    cv2.line(img, tuple(tl), tuple(tr), (255, 0, 0), 4)
    cv2.line(img, tuple(tl), tuple(bl), (255, 0, 0), 4)
    cv2.putText(img,text,center,cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2)
    breath = np.linalg.norm(tl - tr)
    length = np.linalg.norm(tl - bl)
    return breath, length

'''only one test image test.JPG,
 reference and test objects are differentiated by giving different color ranges'''

#getting the camera detected pixel values for the reference object
ref_breath,ref_length=image_processing(lw_red,up_red,"Reference")
print "pixel values...",ref_breath,ref_length

#here is where you give your test data....
test_breath,test_length=image_processing(lw_blue,up_blue,"Test")
print test_breath,test_length
cv2.imshow('live', img)
cv2.waitKey(0)

'''RUN IT AND SEE, it will only proceed after you exit the window'''

#now you can give length and breadth of the reference object in centimeters, since I dint hardcode it.
actual_len=input("enter actual length of the reference object...")#eg. 10
actual_bre=input("enter actual breath of the reference object...")#eg. 8

#calculation
unknown_obj_len=(actual_len*test_length)/ref_length
unknown_obj_bth=(actual_bre*test_breath)/ref_breath
area=(unknown_obj_bth*unknown_obj_len)

#displaying output
print "length of the test object is..",unknown_obj_len,"cm"
print "breath of the test object is......",unknown_obj_bth,"cm"
print "total area of the test object.....",area,"cm2"

#giving accuracy of prediction
x=raw_input("Do you want accuracy of the prediction?...yes or no")
if x=="yes":
    length=input("enter object length")
    breath=input("enter object breath")
    print "length accuracy....",(length/unknown_obj_len)*100
    print "breath accuracy....",(breath/unknown_obj_bth)*100
    print "area accuracy....",((length*breath)/area)*100
if x=="no":
    quit()



'''U. girl, there is also an another method to estimate the size of the object.
If we know the focal length of our camera and distance between the object and camera,
we can form a pythogorian triangle.
from which we can calculate the size of the object by using some cosine laws '''

'''If you need that one, I'll comment the functionalties and send it like this one'''

'''Any new ideas?, please tell me!!'''


